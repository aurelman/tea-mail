# frozen_string_literal: true

module Messages

  # In charge of creating a valid Message from a template reference
  # and valid from and to email addresses
  class Creation
    extend ::LightService::Organizer

    aliases found_template: :template

    def self.call(params)
      with(params).reduce(actions)
    end

    def self.actions
      [
        Templates::FindOne,
        Messages::Build
      ]
    end
  end
end
