# Global helpers
module ApplicationHelper
  def page_title(title = nil)
    [title, 'TeaMail'].reject(&:blank?).join(' | ')
  end
end
