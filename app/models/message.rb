# frozen_string_literal: true

class Message < ApplicationRecord
  belongs_to :template

  validates :template, presence: true
  validates :body, presence: true
  validates :from, presence: true, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :to, presence: true, format: { with: URI::MailTo::EMAIL_REGEXP }
end
