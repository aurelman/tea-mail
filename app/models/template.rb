# Defines for an email template
# Email template are basically some templated content
# * subject
# * body
# Which you can afterward call by replacing its variables by real value
class Template < ApplicationRecord
  extend FriendlyId

  friendly_id :name, use: :slugged

  validates :name, presence: true, uniqueness: true
  validates :subject, presence: true
  validates :body, presence: true
end
