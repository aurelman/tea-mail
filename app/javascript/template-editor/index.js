import 'grapesjs/dist/css/grapes.min.css';
import 'grapesjs-preset-newsletter/dist/grapesjs-preset-newsletter.css';

import 'grapesjs-preset-newsletter';
import grapesjs from 'grapesjs';

const editor = grapesjs.init({
    // Indicate where to init the editor. You can also pass an HTMLElement
    container: '.template-editor',
    // Get the content for the canvas directly from the element
    // As an alternative we could use: `components: '<h1>Hello World Component!</h1>'`,
    fromElement: true,
    // Size of the editor
    // height: '300px',
    // width: 'auto',
    // Disable the storage manager for the moment
    storageManager: false,
    // Avoid any default panel
    // panels: {defaults: []},
    // Avoid the confirmation popup when the form where the editor is embedded is submitted
    noticeOnUnload: false,

    plugins: ['gjs-preset-newsletter'],
    pluginsOpts: {
        'gjs-preset-newsletter': {
            modalTitleImport: 'Import template',
        }
    }
});


function installTemplateEditor() {
    const foundResult = document.getElementsByClassName('with-template-editor');
    if (foundResult.length > 0) {
        foundResult.item(0).addEventListener('submit', () => {
            document.getElementById('template_body').value = editor.runCommand('gjs-get-inlined-html')
        });
    }
}

installTemplateEditor();

document.addEventListener('turbolinks:load', () => {
    // Supposing this file is at the end of the <body>, then :
    // * when coming to the page where this is file is loaded from a link
    //   turbolinks:load is being fired before this file is actually loaded and executed.
    // * When a page is "fully" loaded e.g. via a refresh, Turbolinks fires turbolinks:load
    //   on DOMContentLoaded, so in this case it will wait for scripts to load.
    //
    // TL;DR : If you're loading some scripts at the end of the <body> element,
    // you may not need to listen out for turbolinks:load;
    // the DOM should be accessible without having to wait for the load event.
    //
    // installTemplateEditor();
});
