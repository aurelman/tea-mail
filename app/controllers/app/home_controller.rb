# frozen_string_literal: true

module App

  # Controller in charge of displaying the home page
  class HomeController < ApplicationController
    def index; end
  end

end
