# frozen_string_literal: true

module Api

  # Answers to every requests pointing to `/api/templates/:template_id/messages/`
  class MessagesController < ApiController

    # Does not bring added value compared to the default rails behavior but allows to test
    # http status code in tests instead of exceptions
    rescue_from ActiveRecord::RecordInvalid, with: -> { head :bad_request }

    def create

      result = Messages::Creation.call(
        template_ref: message_params[:template_id],
        from: message_params[:from],
        to: message_params[:to],
        context: message_params[:context].to_h
      )

      if result.failure?
        head :not_found
        return
      end

      message = result.built_message

      if message.valid?
        message.save!
        MessageMailer.prepare(message).deliver_later
        head :ok
      else
        render status: :bad_request, json: message.errors
      end

    end

    private

    def message_params
      params.permit(:template_id, :from, :to, context: {})
    end

  end
end
