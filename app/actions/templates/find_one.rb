# frozen_string_literal: true

module Templates

  # Searches and returns a Template by its id or its friendly id
  class FindOne
    extend ::LightService::Action

    expects :template_ref
    promises :template

    executed do |ctx|
      begin
        ctx.template = Template.friendly.find(ctx.template_ref)
      rescue ActiveRecord::RecordNotFound
        ctx.fail_and_return!('template does not exist')
      end

    end

  end
end
