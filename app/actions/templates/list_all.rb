# frozen_string_literal: true

module Templates

  # Returns every Template instances existing in database
  class ListAll
    extend ::LightService::Action

    promises :templates

    executed do |ctx|
      ctx.templates = Template.all.order(:name)
    end

  end
end
