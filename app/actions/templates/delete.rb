# frozen_string_literal: true

module Templates

  # Deletes from repository the specified Template
  class Delete
    extend ::LightService::Action

    expects :template

    executed do |ctx|
      ctx.template.destroy
    end
  end
end
