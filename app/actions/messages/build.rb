# frozen_string_literal: true

module Messages

  # Builds a Message from a specified Template and params. If context variables are given then the template will be
  # computed against those variables.
  class Build
    extend ::LightService::Action

    expects :template, :to, :from
    promises :built_message

    executed do |ctx|
      ctx.built_message = Message.new.tap do |m|
        m.template = ctx.template
        m.from = ctx.from
        m.to = ctx.to

        parsed = Liquid::Template.parse(ctx.template.body)
        m.body = parsed.render(ctx[:context] || {})
      end
    end

  end
end
