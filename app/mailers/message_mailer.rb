# frozen_string_literal: true

# Used to actually send emails corresponding to the specified Message

# MessageMailer allows to send email from a specified Message.
# MessageMailer doesn't require any template in the +views/+ directory,
# email body is totally dynamic and build from the Message instance.
class MessageMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.message_mailer.prepare.subject
  #
  def prepare(message)
    mail(
      from: message.from,
      to: message.to,
      subject: message.template.subject,
      content_type: 'text/html',
      body: message.body
    )
  end
end
