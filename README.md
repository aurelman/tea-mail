# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

# Contributing

## Prerequisites

`tea-mail` relies on `ruby` `2.7` to run.
 
 In order to contribute more easily you might also need to have docker installed on your local machine.

## Run the project locally

#### Clone the current project

```shell script
git clone git@gitlab.com:aurelman/tea-mail.git
```

#### Run the database

```shell script
cd path/to/team-mail

sudo docker-compose up -d
```

#### Install dependencies

```shell script
bundle install
```

#### Run the application

```shell script
rails s
```


#### Run the tests

If you want to run all the tests at once
```shell script
# from the root path of the project
rspec
```

During the development process, `guard` can automatically run the tests related to the piece of code you are modifying. 
Just launch `guard` in a dedicated separated CLI :

```shell script
# from the root path of the project
guard
```