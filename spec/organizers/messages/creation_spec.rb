# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Messages::Creation, type: :organizer do
  subject { described_class.call(ctx) }

  context 'when a template exists' do
    let!(:template) { create(:template) }

    context 'when calling with proper context' do
      let(:ctx) do
        {
          template_ref: template.friendly_id,
          from: 'from@mail.com',
          to: 'to@mail.com'
        }
      end

      it 'is expected to be successful' do
        expect(subject).to be_a_success
      end

      it 'is expected to return a valid message' do
        expect(subject.built_message).not_to be_nil
        expect(subject.built_message.template).to eq(template)
        expect(subject.built_message.from).to eq('from@mail.com')
        expect(subject.built_message.to).to eq('to@mail.com')
      end

    end

  end
end
