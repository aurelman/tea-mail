# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/api/templates/:id/messages', type: :request do

  describe 'when called' do

    let!(:existing_template) { create(:template, name: 'existing template') }

    describe 'with no template name' do
      it 'returns a 404 error' do
        expect { post '/api/templates/messages', params: {} }.to raise_error(ActionController::RoutingError)

        # expect(response).to have_http_status(:not_found)
        # expect(response.content_type).to eq('application/json; charset=utf-8')
        # expect(JSON.parse(response.body)).to eq({ 'errors' => { 'template' => ["can't be blank"] } })
      end
    end

    describe 'with a non existing template' do
      let(:message_params) { { from: 'from@mail.com', to: 'to@mail.com' } }

      it 'returns a 403' do
        post '/api/templates/non-existing-template/messages', params: message_params

        expect(response).to have_http_status(:not_found)
      end
    end

    describe 'with an invalid body' do

      let(:message_params) { { from: 'from.mail.com', to: 'to.mail.com' } }

      it 'returns a 400' do
        post '/api/templates/existing-template/messages', params: message_params

        expect(response).to have_http_status(:bad_request)
        expect(JSON.parse(response.body)).to match({ 'to' => ['is not a valid email address'], 'from' => ['is not a valid email address'] })
      end

    end

    describe 'with a simple valid body' do

      let(:message_params) { { from: 'from@mail.com', to: 'to@mail.com' } }

      it 'returns a 200' do
        post '/api/templates/existing-template/messages', params: message_params

        expect(response).to have_http_status(:ok)
      end

      it 'creates and saves a message' do
        expect { post '/api/templates/existing-template/messages', params: message_params }.to change(Message, :count).by(1)
        expect(Message.first.template).to eq(existing_template)
      end

      it 'enqueues an email to be sent with the created message' do
        expect { post '/api/templates/existing-template/messages', params: message_params }
          .to have_enqueued_mail(MessageMailer, :prepare).with(Message.first)
      end

    end

    describe 'with a valid body with variables' do

      let!(:template_with_variables) { create(:template, name: 'with variables', body: 'A body with a simple {{variable}}') }
      let(:message_params) { { from: 'from@mail.com', to: 'to@mail.com', context: { 'variable' => 'value' } } }

      it 'returns a 200' do
        post '/api/templates/with-variables/messages', params: message_params

        expect(response).to have_http_status(:ok)
      end

      it 'creates and saves a message' do
        expect { post '/api/templates/with-variables/messages', params: message_params }.to change(Message, :count).by(1)
        expect(Message.first.template).to eq(template_with_variables)
      end

      it 'computes the expanded body' do
        post '/api/templates/with-variables/messages', params: message_params
        expect(Message.first.body).to eq('A body with a simple value')
      end

      it 'enqueues an email to be sent with the created message' do
        expect { post '/api/templates/with-variables/messages', params: message_params }
          .to have_enqueued_mail(MessageMailer, :prepare).with(Message.first)
      end

    end
  end
end
