# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'creating a template', type: :system do

  context 'when not logged in' do
    it 'is not allowed to create a new template' do
      visit(new_template_path)
      expect(page).to have_current_path(user_session_path, ignore_query: true)
    end
  end

  context 'when logged in' do

    before do
      sign_in(create(:user))
    end

    it 'allows to specify its name, subject and content', js: true do
      pending 'cannot implement because of template editor'
      visit(new_template_path)

      fill_in('Name', with: 'Template name')
      fill_in('Subject', with: 'Template subject')


      source = find('div[title="Text"]')
      target = find('.gjs-cv-canvas')

      dragged = source.drag_to(target, delay: 0.5)

      # fill_in('Body', with: 'Template content')

      click_button('Create this template')

      expect(page).to have_content 'Template was successfully created'
      expect(page).to have_content 'Template name'
      expect(page).to have_content 'template-name'
      expect(page).to have_content 'Template subject'
      expect(page).to have_content 'Template content'
    end
  end

end
