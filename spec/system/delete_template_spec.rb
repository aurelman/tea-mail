# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Deleting a template', type: :system do

  context 'when not logged in' do
    it 'is not allowed to delete a template' do
      visit(templates_path)
      expect(page).to have_current_path(user_session_path, ignore_query: true)
    end
  end

  context 'when logged in' do

    before do
      sign_in(create(:user))
    end

    context 'when there are templates displayed' do

      before do
        create(:template)
      end

      it 'is allowed to delete one of them' do
        visit(templates_path)

        page.accept_alert 'Are you sure?' do
          click_link('Destroy')
        end

        expect(page).to have_content 'Template was successfully destroyed'
      end
    end
  end
end
