# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'listing templates', type: :system do

  context 'when not logged in' do
    it 'is not allowed to view list of templates' do
      visit(templates_path)
      expect(page).to have_current_path(user_session_path, ignore_query: true)
    end
  end

  context 'when logged in' do

    before do
      sign_in( create(:user))
      create(:template, name: 'C template')
      create(:template, name: 'A template')
      create(:template, name: 'B template')
    end

    it 'displays templates ordered by name' do
      visit(templates_path)

      within('tbody') do
        expect(page).to have_selector('tr:nth-child(1) td[data-label="Name"]', text: 'A template')
        expect(page).to have_selector('tr:nth-child(2) td[data-label="Name"]', text: 'B template')
        expect(page).to have_selector('tr:nth-child(3) td[data-label="Name"]', text: 'C template')
      end
    end
  end
end
