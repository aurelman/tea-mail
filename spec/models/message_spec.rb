# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Message, type: :model do

  it 'does not allow :to to be empty' do
    message = build(:message, to: nil)

    expect(message).not_to be_valid
    expect(message.errors[:to]).to include("can't be blank")
  end

  it 'does not allow :template to be empty' do
    message = build(:message, template: nil)

    expect(message).not_to be_valid
    expect(message.errors[:template]).to include("can't be blank")
  end

  it 'does not allow :from to be empty' do
    message = build(:message, from: nil)

    expect(message).not_to be_valid
    expect(message.errors[:from]).to include("can't be blank")
  end

  it 'does not allow :body to be empty' do
    message = build(:message, body: nil)

    expect(message).not_to be_valid
    expect(message.errors[:body]).to include("can't be blank")
  end

  it 'does not allow a not valid email address for the :to field' do
    message = build(:message, to: 'not_an_email')

    expect(message).not_to be_valid
    expect(message.errors[:to]).to include('is not a valid email address')
  end

  it 'does not allow a not valid email address for the :from field' do
    message = build(:message, from: 'not_an_email')

    expect(message).not_to be_valid
    expect(message.errors[:from]).to include('is not a valid email address')
  end

  it 'validates a fully built message instance' do
    message = build(:message)

    expect(message).to be_valid
  end
end
