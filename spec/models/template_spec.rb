# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Template, type: :model do
  it 'accepts :name to be empty' do
    template = build(:template, name: nil)

    expect(template).not_to be_valid
    expect(template.errors[:name]).to include("can't be blank")
  end

  it 'does not allow :subject to be empty' do
    template = build(:template, subject: nil)

    expect(template).not_to be_valid
    expect(template.errors[:subject]).to include("can't be blank")
  end

  it 'does not allow :body to be nil' do
    template = build(:template, body: nil)

    expect(template).not_to be_valid
    expect(template.errors[:body]).to include("can't be blank")
  end

  context 'with an already saved instance' do

    before { create(:template, name: 'Back to the future') }

    it 'does not accept to save a new template with an already existing :name' do
      template = build(:template, name: 'Back to the future')

      expect(template).not_to be_valid
      expect(template.errors[:name]).to include('has already been taken')
    end
  end

  it 'generates a friendly_id based on name' do
    template = create(:template, name: 'Back to the future')

    expect(template.friendly_id).to eq('back-to-the-future')
  end

end
