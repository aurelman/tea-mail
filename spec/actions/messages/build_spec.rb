# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Messages::Build, type: :action do
  subject { described_class.execute(ctx) }

  context 'with a Template and context' do
    let(:template) { build(:template, body: 'A simple body') }

    let(:ctx) do
      {
        template: template,
        to: 'to@mail.com',
        from: 'from@mail.com',
        context: {}
      }
    end

    it 'is expected to be successful' do
      expect(subject).to be_a_success
    end

    it 'returns a fully built message' do
      expect(subject.built_message).not_to be_nil
      expect(subject.built_message.template).to eq(template)
      expect(subject.built_message.body).to eq('A simple body')
      expect(subject.built_message.from).to eq('from@mail.com')
      expect(subject.built_message.to).to eq('to@mail.com')
    end
  end

  context 'with a Template with variables and context' do
    let(:template) { build(:template, body: 'A simple body with {{variable}}') }

    let(:ctx) do
      {
        template: template,
        to: 'to@mail.com',
        from: 'from@mail.com',
        context: { 'variable' => 'value' }
      }
    end

    it 'is expected to be successful' do
      expect(subject).to be_a_success
    end

    it 'returns a fully built message' do
      expect(subject.built_message).not_to be_nil
      expect(subject.built_message.template).to eq(template)
      expect(subject.built_message.body).to eq('A simple body with value')
      expect(subject.built_message.from).to eq('from@mail.com')
      expect(subject.built_message.to).to eq('to@mail.com')
    end
  end
end
