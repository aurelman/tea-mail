# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Templates::Delete, type: :action do

  subject { described_class.execute(ctx) }

  context 'when executed' do

    let!(:template) { create(:template) }

    let(:ctx) { { template: template } }

    it 'is expected to be successful and deletes the specified template from database' do
      expect(subject).to be_a_success
      expect(Template.count).to eq(0)
    end

  end
end
