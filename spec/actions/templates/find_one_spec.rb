# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Templates::FindOne, type: :action do
  subject(:action) { described_class.execute(ctx) }

  context 'when the searched template does not exist' do
    context 'when searching it by its id' do
      let(:ctx) { { template_ref: 'random-uid' } }

      it 'is expected not to be successful' do
        expect(action).not_to be_a_success
      end

      it 'is expected to return an appropriate message' do
        expect(action.message).to eq('template does not exist')
      end
    end

    context 'when searching it by its friendly name' do
      let(:ctx) { { template_ref: 'back-to-the-future' } }

      it 'is expected not to be successful' do
        expect(action).not_to be_a_success
      end

      it 'is expected to return an appropriate message' do
        expect(action.message).to eq('template does not exist')
      end
    end
  end

  context 'when the searched template exists' do
    let!(:template) { create(:template, name: 'Back To The Future') }

    context 'when searching it by its id' do
      let(:ctx) { { template_ref: template.id } }

      it 'is expected to be successful' do
        expect(action).to be_a_success
      end

      it 'is expected to return the actual template' do
        expect(action.template).to eq(template)
      end
    end

    context 'when searching it by its friendly name' do
      let(:ctx) { { template_ref: 'back-to-the-future' } }

      it 'is expected to be successful' do
        expect(action).to be_a_success
      end

      it 'is expected to return the actual template' do
        expect(action.template).to eq(template)
      end
    end

  end
end
