# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Templates::ListAll, type: :action do
  subject { described_class.execute(ctx) }

  context 'when some templates exist' do

    let!(:template_c) { create(:template, name: 'C Template') }
    let!(:template_a) { create(:template, name: 'A Template') }
    let!(:template_b) { create(:template, name: 'B Template') }

    let(:ctx) { {} }

    it 'is expected to be successful' do
      expect(subject).to be_a_success
    end

    it 'is returns every of the existing templates' do
      expect(subject.templates).to contain_exactly(template_a, template_b, template_c)
    end

    it 'returns template ordered by names' do
      templates = subject.templates
      expect(templates[0]).to eq(template_a)
      expect(templates[1]).to eq(template_b)
      expect(templates[2]).to eq(template_c)
    end
  end
end
