# frozen_string_literal: true

FactoryBot.define do
  factory :template do
    sequence(:name) { |i| "Template #{i}" }
    subject { 'Any relevant subject' }

    body { 'Any relevant body' }
  end
end
