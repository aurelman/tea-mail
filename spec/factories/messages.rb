# frozen_string_literal: true

FactoryBot.define do
  factory :message do
    sequence(:to) { |i| "to_#{i}@test.com" }
    sequence(:from) { |i| "from_#{i}@test.com" }
    template
    body { 'A simple body' }
  end
end
