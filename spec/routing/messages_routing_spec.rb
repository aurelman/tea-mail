# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::MessagesController, type: :routing do
  describe 'routing' do
    it 'routes to #post' do
      expect(post: '/api/templates/1/messages').to route_to('api/messages#create', template_id: '1')
    end
  end
end
