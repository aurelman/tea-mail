# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DeviseController, type: :routing do
  describe 'routing' do
    it 'routes to #sign_up' do
      expect(get: '/users/signup').to route_to('devise/registrations#new')
    end

    it 'routes to #sign_in' do
      expect(get: '/users/login').to route_to('devise/sessions#new')
    end

    it 'routes to #sign_out' do
      expect(delete: '/users/logout').to route_to('devise/sessions#destroy')
    end
  end
end
