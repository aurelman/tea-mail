# frozen_string_literal: true

require 'rails_helper'

RSpec.describe App::TemplatesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/templates').to route_to('app/templates#index')
    end

    it 'routes to #new' do
      expect(get: '/templates/new').to route_to('app/templates#new')
    end

    it 'routes to #show' do
      expect(get: '/templates/1').to route_to('app/templates#show', id: '1')
    end

    it 'routes to #edit' do
      # The edition of a template is handled in the show action
      expect(get: '/templates/1/edit').not_to be_routable
    end

    it 'routes to #create' do
      expect(post: '/templates').to route_to('app/templates#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/templates/1').to route_to('app/templates#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/templates/1').to route_to('app/templates#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/templates/1').to route_to('app/templates#destroy', id: '1')
    end
  end
end
