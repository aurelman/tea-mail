# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MessageMailer, type: :mailer do
  describe 'prepare' do

    let(:mail) { described_class.prepare(message) }

    describe 'with a simple body' do
      let(:message) { build(:message) }

      it 'renders the headers' do
        expect(mail.subject).to eq(message.template.subject)
        expect(mail.to).to eq([message.to])
        expect(mail.from).to eq([message.from])
      end

      it 'renders the body' do
        expect(mail.body.encoded).to match(message.body)
      end
    end
  end

end
