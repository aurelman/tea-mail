# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/message
class MessagePreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/message/prepare
  def prepare
    MessageMailer.prepare(Message.first)
  end

end
