# frozen_string_literal: true

require 'rails_helper'

describe ApplicationHelper, type: :helper do

  describe '#page_title' do
    it 'displays a default title if none is specified' do
      expect(page_title).to eq('TeaMail')
    end

    it 'displays a default title if an empty string is specified' do
      expect(page_title(' ')).to eq('TeaMail')
    end

    it 'concatenates default title with the one specified' do
      expect(page_title('Any')).to eq('Any | TeaMail')
    end
  end
end
