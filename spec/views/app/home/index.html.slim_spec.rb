# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'app/home/index', type: :view do
  describe 'the navigation menu' do
    it 'displays a link to my template' do
      render template: 'app/home/index', layout: 'layouts/application'

      assert_select 'nav a.item[href=?]', templates_path
    end
  end
end
