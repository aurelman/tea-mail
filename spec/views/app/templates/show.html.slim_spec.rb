# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'app/templates/show', type: :view do
  before do
    @template = assign(:template, create(:template, name: 'Name', body: 'MyText'))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/MyText/)
  end
end
