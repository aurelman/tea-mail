# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'app/templates/new', type: :view do
  before do
    assign(:template, Template.new(
      name: 'MyString',
      body: 'MyText'
    ))
  end

  it 'renders new template form' do
    render

    assert_select 'form[action=?][method=?]', templates_path, 'post' do

      assert_select 'input[name=?]', 'template[name]'
    end
  end
end
