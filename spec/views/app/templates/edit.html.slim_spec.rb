# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'app/templates/edit', type: :view do
  before do
    @template = assign(:template, create(:template))
  end

  it 'renders the edit template form' do
    render

    assert_select 'form[action=?][method=?]', template_path(@template), 'post' do

      assert_select 'input[name=?]', 'template[name]'
    end
  end
end
