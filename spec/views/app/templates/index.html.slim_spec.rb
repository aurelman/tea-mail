# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'app/templates/index', type: :view do
  before do
    assign(:templates, [
             create(:template, name: 'name one', subject: 'Subject 1', body: 'First body'),
             create(:template, name: 'name two', subject: 'Subject 2', body: 'Second body')
           ])
  end

  it 'renders a list of templates' do
    render
    assert_select 'tr>td', text: 'name one', count: 1
    assert_select 'tr>td', text: 'name-one', count: 1
    assert_select 'tr>td', text: 'Subject 1', count: 1

    assert_select 'tr>td', text: 'name two', count: 1
    assert_select 'tr>td', text: 'name-two', count: 1
    assert_select 'tr>td', text: 'Subject 2', count: 1
  end
end
