# frozen_string_literal: true

Rails.application.config.action_view.field_error_proc = proc do |html_tag, instance|
  html_tag.html_safe
end
