const { environment } = require('@rails/webpacker')

const webpack = require('webpack')

environment.plugins.prepend('Provide',
    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery'
    })
);

environment.loaders.prepend('less', {
    test: /\.less$/,
    loader: 'less-loader', // compiles Less to CSS
});

module.exports = environment
