# frozen_string_literal: true

Rails.application.routes.draw do

  devise_for :users, path_names: {
    sign_in: 'login',
    sign_out: 'logout',
    sign_up: 'signup'
  }

  scope module: 'app' do

    root to: 'home#index'

    resources :templates, except: [:edit]
  end

  namespace :api do
    resources :templates do
      resources :messages, only: [:create]
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
