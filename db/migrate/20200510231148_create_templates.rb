# frozen_string_literal: true

# Creates the template data model
class CreateTemplates < ActiveRecord::Migration[6.1]
  def change
    create_table :templates, id: :uuid do |t|
      t.string :name, null: false, index: { name: 'uni_templates_name', unique: true }
      t.string :slug, null: false, index: { name: 'uni_templates_slug', unique: true }
      t.string :subject, null: false
      t.text :body, null: false

      t.timestamps
    end
  end
end
