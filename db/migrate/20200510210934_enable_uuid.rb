# Enable UUID extension on postgres database
class EnableUuid < ActiveRecord::Migration[6.1]
  def change
    enable_extension 'pgcrypto' # unless extension_enabled?('pgcrypto')
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
