class CreateMessages < ActiveRecord::Migration[6.1]
  def change
    create_table :messages, id: :uuid do |t|
      t.references :template, null: false, foreign_key: true, type: :uuid
      t.string :to, null: false
      t.string :from, null: false
      t.datetime :sent_at, null: true
      t.timestamps
    end
  end
end
